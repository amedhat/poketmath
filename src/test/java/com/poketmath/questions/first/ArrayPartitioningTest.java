package com.poketmath.questions.first;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
/**
 * 
 * @author Ahmed Othman
 *
 */
@RunWith(Parameterized.class)
public class ArrayPartitioningTest {	
	
	ArrayPartitioning arrayPartitioning = new ArrayPartitioning();
	
	@Parameters
    public static Collection<int[][]> data() {
        return Arrays.asList(new int [][][] {     
                 { {2,4,7,6,1,3,5,4}, {2,4,4,6,1,3,5,7} },
                 { {2,4,7,6,1,3,2,9,1,6,5,4}, {2,4,4,6,6,2,3,9,1,1,5,7} }
           });
    }
    
    @Parameter
	public int[] array;
	
    @Parameter(value = 1)
    public int[] expectedarray;

	@Test
	public void partition_testSimpleInput_SimpleOutput() {
		assertArrayEquals(expectedarray, arrayPartitioning.partition(array));
	}
}
