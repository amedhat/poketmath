package com.poketmath.questions.fourth;

import java.io.IOException;
import java.sql.Timestamp;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Deserializer to convert seconds timestamp to miliseconds timestamp.
 * 
 * @author Ahmed Othman
 *
 */
public class UnixTimestampDeserializer extends JsonDeserializer<Timestamp> {

	@Override
	public Timestamp deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String timestamp = jp.getText().trim();

        try {
            return new Timestamp(Long.valueOf(timestamp + "000"));
        } catch (NumberFormatException e) {
            System.out.println("Unable to deserialize timestamp: " + timestamp + e);
            return null;
        }
    }
}
