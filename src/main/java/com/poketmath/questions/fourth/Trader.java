package com.poketmath.questions.fourth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Ahmed Othman
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Trader implements Comparable<Trader> {
	
	private String name;
	private String city;
	private String id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public int compareTo(Trader trader) {
		return this.getName().toUpperCase().compareTo(trader.getName().toUpperCase());
	}
}
