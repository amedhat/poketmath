package com.poketmath.questions.fourth;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author Ahmed Othman
 *
 */
public class PoketmathRestClient {

	private static final String API_DOMAIN = "https://fvjkpkflnc.execute-api.us-east-1.amazonaws.com";
	private static final String API_PATH = "/prod";

	private static final String TRADERS_ENDPOINT = "/traders";
	private static final String TRANSACTIONS_ENDPOINT = "/transactions";

	private static final String API_KEY_HEADER_NAME = "x-api-key";
	private static final String API_KEY = "gaqcRZE4bd58gSAJH3XsLYBo1EvwIQo88IfYL1L5";

	private List<Transaction> transactionList;

	private List<Trader> traderList;

	public static void main(String... args) {
		PoketmathRestClient poketmathRestClient = new PoketmathRestClient();

		List <Trader> cityTraderList = poketmathRestClient.getTraderListByCity(poketmathRestClient.traderList, "Singapore");
		System.out.println("All traders from Singapore and sort them by name.");
		for (Trader trader: cityTraderList) {
			System.out.println(trader.getId() + "  " + trader.getName() + "  " + trader.getCity());
		}

		Transaction maxTransaction = poketmathRestClient.getHighestTransaction(poketmathRestClient.transactionList);
		
		System.out.println("The transaction with the highest value.");
		System.out.println("The max transaction value is: " + maxTransaction.getTimestamp() + "  " + maxTransaction.getTraderId() + "  " + maxTransaction.getValue());

		DateFormat formatter;
	    formatter = new SimpleDateFormat("yyyy");
	    Date date = null;
	    try {
			date = formatter.parse("2016");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	    List<Transaction> yearlyTransactionList = poketmathRestClient.getTransactionListByYear(poketmathRestClient.transactionList, new Timestamp(date.getTime()));
		
	    System.out.println("All transactions in the year 2016 and sort them by value (high to small).");
		for (Transaction transaction : yearlyTransactionList) {
			System.out.println(
					transaction.getTimestamp() + "  " + transaction.getTraderId() + "  " + transaction.getValue());
		}

		Double average = poketmathRestClient.getAverageTransactionValueByTraderCity(poketmathRestClient.transactionList,
				poketmathRestClient.traderList, "Beijing");
		
		System.out.println("The average of transactions' values from the traders living in Beijing.");
		System.out.println("The average is: " + average);

	}

	PoketmathRestClient() {
		this.traderList = getTraders();
		this.transactionList = getTransactions();
	}

	/**
	 * Method to find all traders from a give city and sort them by name.
	 * 
	 * @param traderList
	 * @param city ex Singapore
	 * @return list of traders from the given city.
	 */
	public List<Trader> getTraderListByCity(List<Trader> traderList, String city) {
		
		// We can use BST instead but for simplicity we will just use a list and sort it.
		List <Trader> cityTraderList = new ArrayList<Trader>();
		
		
		for (Trader trader: traderList) {
			if (trader != null && trader.getCity() != null && trader.getCity().equals(city) ){
				cityTraderList.add(trader);
			}
		}
		Collections.sort(cityTraderList);
		return cityTraderList;
	}

	/**
	 * Method to find the transaction with the highest value.
	 * 
	 * @param transactionList
	 * @return
	 */
	public Transaction getHighestTransaction(List<Transaction> transactionList) {
		Transaction maxTransaction = new Transaction();
		maxTransaction.setValue(0.0d);
		
		for (Transaction transaction: transactionList) {
			if (transaction.getValue() > maxTransaction.getValue()) { 
				maxTransaction = transaction;
			}
		}
		return maxTransaction;
	}

	/**
	 * Method to find all transactions in the year 2016 and sort them by value (high to small).
	 * 
	 * @param transactionList
	 * @param year
	 * @return
	 */
	public List<Transaction> getTransactionListByYear(List<Transaction> transactionList, Timestamp year) {
		List<Transaction> yearlyTransactionList = new ArrayList<>();
		for (Transaction transaction: transactionList) {
			Timestamp timestamp = transaction.getTimestamp();
			Calendar transactionCal = Calendar.getInstance();
			transactionCal.setTime(timestamp);
			Calendar yearCal = Calendar.getInstance();
			yearCal.setTime(year);
			
			if (transactionCal.get(Calendar.YEAR) == yearCal.get(Calendar.YEAR)) {
				yearlyTransactionList.add(transaction);
			}
		}
		Collections.sort(yearlyTransactionList);
		return yearlyTransactionList;
	}

	/**
	 * The average of transactions' values from the traders living in the given City.
	 * @param transactionList
	 * @param traderList
	 * @param city
	 * @return
	 */
	public Double getAverageTransactionValueByTraderCity(List<Transaction> transactionList,
			List<Trader> traderList, String city) {
		Double transactionsSum = 0.0d;
		Integer transactionCount = 0;
		
		Map<String, Boolean> traderIdMap = new HashMap<String, Boolean> ();
		
		for (Trader trader: traderList) {
			if (trader.getCity().equals(city)) {
				traderIdMap.put(trader.getId(), true);
			}
		}
		
		for (Transaction transaction: transactionList) {
			if (traderIdMap.containsKey(transaction.getTraderId())) {
				transactionsSum += transaction.getValue();
				transactionCount++;
			}
		}
		return transactionsSum/transactionCount;
	}

	private static List<Trader> getTraders() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set(API_KEY_HEADER_NAME, API_KEY);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);

		ResponseEntity<List<Trader>> traderListResponse = restTemplate.exchange(
				API_DOMAIN + API_PATH + TRADERS_ENDPOINT, HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<List<Trader>>() {
				});

		return traderListResponse.getBody();
	}

	private static List<Transaction> getTransactions() {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set(API_KEY_HEADER_NAME, API_KEY);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);

		ResponseEntity<List<Transaction>> transactionListResponse = restTemplate.exchange(
				API_DOMAIN + API_PATH + TRANSACTIONS_ENDPOINT, HttpMethod.GET, httpEntity,
				new ParameterizedTypeReference<List<Transaction>>() {
				});

		return transactionListResponse.getBody();

	}
}
