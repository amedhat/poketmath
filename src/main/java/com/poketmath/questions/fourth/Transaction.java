package com.poketmath.questions.fourth;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * 
 * @author Ahmed Othman
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction implements Comparable<Transaction> {
	
	@JsonDeserialize(using = UnixTimestampDeserializer.class)
	public Timestamp timestamp;
	
	private String traderId;
	private Double value;
	
	public Timestamp getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getTraderId() {
		return traderId;
	}
	public void setTraderId(String traderId) {
		this.traderId = traderId;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	
	@Override
	public int compareTo(Transaction transaction) {
		return transaction.getValue().compareTo(this.getValue());
	}
}
