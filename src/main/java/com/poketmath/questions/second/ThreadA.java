package com.poketmath.questions.second;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreadA implements Runnable {

	private Foo foo;
	
	private final CyclicBarrier cyclicBarrier;
	

	public ThreadA(Foo foo, CyclicBarrier cyclicBarrier) {
		this.foo = foo;
		this.cyclicBarrier = cyclicBarrier;
	}

	@Override
	public void run() {
		try {
			cyclicBarrier.await();
		} catch (InterruptedException e) {
			System.out.println("Thread A interrupted!");
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			System.out.println("Thread A interrupted!");
			e.printStackTrace();
		}
		
		foo.first();
		
		foo.firstLatch.countDown();
	}
}
