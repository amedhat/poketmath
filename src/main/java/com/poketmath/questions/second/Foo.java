package com.poketmath.questions.second;

import java.util.concurrent.CountDownLatch;

/**
 * 
 * @author Ahmed Othman
 * 
 *         Question: The same instance of Foo will be passed to three different
 *         threads. Thread A will call first(), Thread B will call second(), and
 *         Thread C will call third(). Design a mechanism to ensure that first()
 *         is called before second() and second() is called before third().
 *
 */
public class Foo {
	
	// CountDownLatch is advanced locking to make sure that the second method is waiting for 
	CountDownLatch firstLatch = new CountDownLatch(1);
	CountDownLatch secondLatch = new CountDownLatch(1);

	
	Foo() {
		System.out.println("Foo");
	}

	void first() {
		System.out.println("First");
	}

	void second() {
		System.out.println("Second");
	}

	void third() {
		System.out.println("Third");
	}
	
}
