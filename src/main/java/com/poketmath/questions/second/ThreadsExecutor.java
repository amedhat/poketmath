package com.poketmath.questions.second;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 
 * @author Ahmed Othman
 *
 *         Question: The same instance of Foo will be passed to three different
 *         threads. Thread A will call first(), Thread B will call second(), and
 *         Thread C will call third(). Design a mechanism to ensure that first()
 *         is called before second() and second() is called before third().
 */
public class ThreadsExecutor {

	public static void main(String[] args) {
		final Foo foo = new Foo();
		
		// To make sure the all the threads started almost at the same time.
		final CyclicBarrier barrier = new CyclicBarrier(4);

		
		Thread threadA = new Thread(new ThreadA(foo, barrier));
		Thread threadB = new Thread(new ThreadB(foo, barrier));
		Thread threadC = new Thread(new ThreadC(foo, barrier));

		threadA.start();
		threadB.start();
		threadC.start();
		
		
		try {
			barrier.await();
		} catch (InterruptedException e) {
			System.out.println("Main Thread interrupted!");
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			System.out.println("Main Thread interrupted!");
			e.printStackTrace();
		}
	}
}
