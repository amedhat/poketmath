package com.poketmath.questions.second;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreadB implements Runnable{

	private Foo foo;
	private final CyclicBarrier cyclicBarrier;

	public ThreadB(Foo foo, CyclicBarrier cyclicBarrier) {
		this.foo = foo;
		this.cyclicBarrier = cyclicBarrier;
	}

	@Override
	public void run() {
		try {
			cyclicBarrier.await();
		} catch (InterruptedException e) {
			System.out.println("Thread B interrupted!");
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			System.out.println("Thread B interrupted!");
			e.printStackTrace();
		}
		
		try {
			foo.firstLatch.await();
		} catch (InterruptedException e) {
			System.out.println("Interrupted!");
			e.printStackTrace();
		}
		
		foo.second();
		
		foo.secondLatch.countDown();
	}

}
