package com.poketmath.questions.third;

import java.util.Stack;
/**
 * 
 * @author Ahmed Othman
 *
 * @param <T>
 */
public class MyQueue<T> {
	Stack<T> firstStack = new Stack<T>();
	Stack<T> secondStack = new Stack<T>();
	
	public void queue(T item) {
		firstStack.push(item);
	}
	
	public T dequeue() {
		if (secondStack.isEmpty()) {
			while (firstStack.isEmpty()) {
				secondStack.push(firstStack.pop());
			}
		}
		return secondStack.pop();
	}
}
