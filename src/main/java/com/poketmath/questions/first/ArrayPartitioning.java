package com.poketmath.questions.first;

/**
 * 
 * @author Ahmed Othman
 * 
 *         Question: Take an array of integers and partition it so that all the
 *         even integers in the array precede all the odd integers in the array.
 *         Your solution must take linear time in the size of the array and
 *         operate in-place with only a constant amount of extra space. sample
 *         input : 2,4,7,6,1,3,5,4 sample output : 2,4,6,4,7,1,3,5
 *
 */
public class ArrayPartitioning {
	
	public int[] partition(int[] array) {
		
		int n = array.length;
		
		// pointer to the end of the array
		int j = n-1;
		
		// temp variable used for swapping elements in the array
		int tmp;
		
		for (int i =0; i < array.length; i++) {
			if (array[i] %2 != 0) {
				while (i<j) {
					if(array[j] % 2 == 0) {
						tmp = array[j]; 
						array[j] = array[i];
						array[i] = tmp;
						j--;
						break;
					}
					j--;
				}
				if (i>=j) break;
			}
		}
		return array;
	}
}
