*README*
--------
This is the solutions for the pre-interview test
https://docs.google.com/document/d/1-dEBBDFibdiAF27JOPq01sVBjjGcU-eO5fYflOqFlLk/edit

This gradle project has all the solutions each problem has its own package

1- First problem, you will find the solutions under the following package: com.poketmath.questions.first for this problem it has unit test class ArrayPartitioningTest.

2- Second problem, you will find the solution under the following package: com.poketmath.questions.second

3- Third problem, you will find the solution under the following package: com.poketmath.questions.third


4- Fourth problem, you will find the solution under the following package: com.poketmath.questions.fourth
to be able to run the solution, you need to have gradle installed and all what you need is to the following from the command line:

$ gradle clean build run

or 

$gradle clean build
$java -jar build/libs/poketmath.jar


Note:
There are no unit tests for the last 3 problems as solving them took almost all the time specified for the test.
if you like more unit tests to be add, please let me know.